import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDetailPageComponent } from './admin-detail-page.component';

describe('AdminDetailPageComponent', () => {
  let component: AdminDetailPageComponent;
  let fixture: ComponentFixture<AdminDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
