import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RouterLink, Router } from '@angular/router';


@Component({
  selector: 'app-admin-detail-page',
  templateUrl: './admin-detail-page.component.html',
  styleUrls: ['./admin-detail-page.component.css']
})
export class AdminDetailPageComponent implements OnInit {

  public title = "Lista de usuarios"
  displayedColumns: string[] = ['name', 'email','options'];
  public dataSource;
  logged;
  public user;
  public admin;
  constructor(private httpClient: HttpClient,private router: Router) { }

  ngOnInit() {
    this.httpClient.get('http://localhost:8081/allUsers').subscribe((res : any[])=>{
        this.dataSource = res;
    });
    let localstoreage = localStorage.getItem("logged");
    if( localstoreage == "true"){
      this.logged = true;
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user)
      this.getRole(this.user);
    }
    else{
      this.logged = false;
    }
  }

  deleteUser(id){
    this.httpClient.delete('http://localhost:8081/deleteUser/'+id).subscribe((data)=>{
      window.location.reload();
    });
  }

  login() {
    this.router.navigateByUrl('/login');
  }
  
  register() {
    this.router.navigateByUrl('/register');
  }

  logout() {
    this.logged = false;
    localStorage.setItem("logged","false");
    localStorage.setItem("user","null");
    // window.location.reload();
    this.router.navigateByUrl('/home');
  }

  getRole(user){
    if(user.role == "admin"){
      this.admin = true;
    }
    else{
      this.admin = false;
    }
  }

  home() {
    this.router.navigateByUrl('/home');
  }
}
