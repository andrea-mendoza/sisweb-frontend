import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book';
import { Genre } from '../models/genre';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent implements OnInit {
  public title = "Registrar Libro";
  public listStatus:any = [];
  public listGenre:any = [];
  public book:Book;
  logged;
  public user;
  public admin;

  constructor(private http: HttpClient,private router: Router) {
    // this.book = new Book("","","","","","",null,""); //con imagen
    this.book = new Book("","","","","","",0);
   }

  ngOnInit() {

    this.http.get('http://localhost:8083/allGenres').subscribe((res : any[])=>{
        this.listGenre = res;
    });
    
    this.listStatus[0] = "Publicado";
    this.listStatus[1] = "En Edicion";
    this.listStatus[2] = "No Publicado";

    this.book = new Book("","","","","","",0);
    let localstoreage = localStorage.getItem("logged");
    if( localstoreage == "true"){
      this.logged = true;
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user)
      this.getRole(this.user);
    }
    else{
      this.logged = false;
    } 
  }

  onSubmit(){
    this.http.post(`http://localhost:8082/newBook`,this.book)
    .subscribe(
      (data:any) => {
        this.router.navigateByUrl('/home');
      }
    )
  }

  login() {
    this.router.navigateByUrl('/login');
  }
  
  home() {
    this.router.navigateByUrl('/');
  }

  register() {
    this.router.navigateByUrl('/register');
  }

  getRole(user){
    if(user.role == "admin"){
      this.admin = true;
    }
    else{
      this.admin = false;
    }
  }

  logout() {
    this.logged = false;
    localStorage.setItem("logged","false");
    localStorage.setItem("user","null");
    // window.location.reload();
    this.router.navigateByUrl('/home');
  }
}
