import { Component, OnInit } from '@angular/core';
import { RouterLink, Router } from '@angular/router';
import { IImage } from 'ng-simple-slideshow';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public title = "Inicio";
  logged;
  public user;
  public admin;
  constructor(private router: Router, private httpClient: HttpClient) { }

  ngOnInit() {
  
    let localstoreage = localStorage.getItem("logged");
    if( localstoreage == "true"){
      this.logged = true;
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user)
      this.getRole(this.user);
    }
    else{
      this.logged = false;
    }
  }

  login() {
    this.router.navigateByUrl('/login');
  }
  
  register() {
    this.router.navigateByUrl('/register');
  }

  logout() {
    this.logged = false;
    localStorage.setItem("logged","false");
    localStorage.setItem("user","null");
    window.location.reload();
  }

  getRole(user){
    if(user.role == "admin"){
      this.admin = true;
    }
    else{
      this.admin = false;
    }
  }
}
