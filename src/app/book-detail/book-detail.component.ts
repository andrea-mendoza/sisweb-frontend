import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book';
import { Genre } from '../models/genre';
import { HttpClient } from '@angular/common/http';
import { Cart } from '../models/cart';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {
  panelOpenState = false;
  public books = [];
  public logged;
  public admin;
  public user;

  url = 'http://BuenaLibreriaBoliviana.com';
  text = `En verdad les recomiendo `;
  imageUrl = 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/test/5899d3b75cafe85ef18b4568/test-libros1.jpg';
  conector = ' de ';
  caritas = " >.< "

  constructor(private httpClient: HttpClient) { 
    
  }

  ngOnInit() {
    let localstoreage = localStorage.getItem("logged");
    let actualUser = JSON.parse(localStorage.getItem("user"));
    if( localstoreage == "true"){
      this.logged = true;
      this.getRole(actualUser);
    }
    else{
      this.logged = false;
      this.admin = false;
      this.user = false;
    }
    this.httpClient.get('http://localhost:8082/allBooks').subscribe((res : any[])=>{
        this.books = res;
        this.booksConFormato(res);
    });
  }

  deleteBook(id){
    this.httpClient.delete('http://localhost:8082/deleteBook/'+id).subscribe((data)=>{
      window.location.reload();
    });
  }

  getRole(actualUser){
    if(actualUser.role == "admin"){
        this.admin = true;
        this.user = false;
    }
    else{
      this.user = true;
      this.admin = false;
    }
  }

  booksConFormato(listBooks){
    for(let i=0;i<listBooks.length;i++){
      this.httpClient.get('http://localhost:8083/getGenreById/'+ listBooks[i].id_genre).subscribe((res : any[])=>{
        listBooks[i].id_genre = res;
    });
    }
  }

  addCart(book_id){
    let actualUser = JSON.parse(localStorage.getItem("user"));
    let cart = new Cart(book_id,actualUser.id);
    console.log(cart);
    this.httpClient.post('http://localhost:8084/newCart',cart)
    .subscribe(
      (response:any) => {
        
     },error  => { alert("Error al añadir al carrito");}
    )
  }

}
