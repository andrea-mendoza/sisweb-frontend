import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  public user;
  public title = "Editar Usuario"
  constructor(private httpClient: HttpClient,private route: ActivatedRoute,private router: Router) { }
  logged;
  public admin;

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("user"));
    let localstoreage = localStorage.getItem("logged");
    if( localstoreage == "true"){
      this.logged = true;
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user)
      this.getRole(this.user);
    }
    else{
      this.logged = false;
    }
  }


  onSubmit(){
    let newUser = this.user;
    this.route.paramMap.subscribe(params => {
      this.httpClient.put(`http://localhost:8081/editUser/`+this.route.snapshot.params.id,newUser)
      .subscribe(
        (data:any) => {
          this.router.navigateByUrl('/home');
          localStorage.setItem("user",JSON.stringify(newUser));
        }
      )
    });
  }

  login() {
    this.router.navigateByUrl('/login');
  }
  
  register() {
    this.router.navigateByUrl('/register');
  }

  logout() {
    this.logged = false;
    localStorage.setItem("logged","false");
    localStorage.setItem("user","null");
    // window.location.reload();
    this.router.navigateByUrl('/home');
  }

  getRole(user){
    if(user.role == "admin"){
      this.admin = true;
    }
    else{
      this.admin = false;
    }
  }

  home(){
    this.router.navigateByUrl('/home');
  }
}
