export class User{
    public id:number
    constructor(
        public name:string,
        public password:string,
        public email:string,
        public role:string
    ){}
}