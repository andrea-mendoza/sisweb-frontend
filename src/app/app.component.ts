import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend-final';

  constructor(private router: Router){

  }
  ngOnInit(){
  }

  lista(){
    this.router.navigateByUrl('/book_detail');
  }
}
