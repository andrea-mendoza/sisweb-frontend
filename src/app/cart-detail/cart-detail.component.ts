import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart-detail',
  templateUrl: './cart-detail.component.html',
  styleUrls: ['./cart-detail.component.css']
})
export class CartDetailComponent implements OnInit {

  public title = "Mi carrito"
  displayedColumns: string[] = ['book', 'author','options'];
  public dataSource = [];
  public books;
  public user;
  logged;
  public admin;

  constructor(private router: Router,private httpClient: HttpClient) { 

  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("user"));
    this.httpClient.get('http://localhost:8084/allCartByUser/'+this.user.id).subscribe((res : any[])=>{
        this.dataSource= res;
        this.librosDelUsuario(res);
        // for(let i=0;i<res.length;i++){
        //   if(res[i].user_id == this.user.id){
        //     this.dataSource.push(res[i]);
        //   }
        // }
        // 
        // console.log(this.dataSource);
    });
    let localstoreage = localStorage.getItem("logged");
    if( localstoreage == "true"){
      this.logged = true;
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user)
      this.getRole(this.user);
    }
    else{
      this.logged = false;
    }
  }

  deleteBookCart(id){
    this.httpClient.delete('http://localhost:8084/deleteCart/'+id).subscribe((data)=>{
      window.location.reload();
    });
  }

  librosDelUsuario(cart){
    for(let i =0 ;i<cart.length;i++){
      if(cart[i].user_id == this.user.id){
        this.httpClient.get('http://localhost:8082/getById/'+ cart[i].book_id).subscribe((res : any[])=>{
        cart[i].book_id = res;
        // console.log(cart[i]);
        });
      }
    }
  }

  logout() {
    this.logged = false;
    localStorage.setItem("logged","false");
    localStorage.setItem("user","null");
    window.location.reload();
  }

  getRole(user){
    if(user.role == "admin"){
      this.admin = true;
    }
    else{
      this.admin = false;
    }
  }
  
  home(){
    this.router.navigateByUrl('/home');
  }
}
