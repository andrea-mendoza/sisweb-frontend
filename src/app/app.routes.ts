import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { CreateBookComponent } from './create-book/create-book.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { AdminDetailPageComponent } from './admin-detail-page/admin-detail-page.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { CartDetailComponent } from './cart-detail/cart-detail.component';
import { UploadDataComponent } from './upload-data/upload-data.component';

const APPROUTES: Routes = [
    { path: 'upload_data', component: UploadDataComponent },
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: CreateUserComponent },
    { path: 'book_detail', component: BookDetailComponent },
    { path: 'create_book', component: CreateBookComponent },
    { path: 'edit_book/:id', component: EditBookComponent },
    { path: 'edit_user/:id', component: EditUserComponent },
    { path: 'admin_detail', component: AdminDetailPageComponent},
    { path: 'cart_detail', component: CartDetailComponent},
    { path: 'user_detail', component: UserDetailComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

// tslint:disable-next-line:eofline
export const APP_ROUTER = RouterModule.forRoot(APPROUTES);
