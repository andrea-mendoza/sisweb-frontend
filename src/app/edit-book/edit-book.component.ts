import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../models/book';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {

  public book;
  public title = "Editar Libro";
  public listStatus:any = [];
  public listGenre;
  public actual;
  public date;
  logged;
  public user;
  public admin;

  constructor(private httpClient: HttpClient,private route: ActivatedRoute,private router: Router) { 
    this.route.paramMap.subscribe(params => {
      this.getBook(this.route.snapshot.params.id);
    });

    this.listStatus[0] = "Publicado";
    this.listStatus[1] = "En Edicion";
    this.listStatus[2] = "No Publicado";

    this.httpClient.get('http://localhost:8083/allGenres').subscribe((res : any[])=>{
        this.listGenre = res;
    });

    this.date = new FormControl(new Date());
  }

  ngOnInit() {
    this.book = new Book("","","","","","",0);
    let localstoreage = localStorage.getItem("logged");
    if( localstoreage == "true"){
      this.logged = true;
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user)
      this.getRole(this.user);
    }
    else{
      this.logged = false;
    } 
  }

  getBook(id){
    this.httpClient.get('http://localhost:8082/getById/'+id).subscribe((res : any[])=>{
      this.book = res;
    });
  }

  onSubmit(){
    this.route.paramMap.subscribe(params => {
      this.httpClient.put(`http://localhost:8082/editBook/`+this.route.snapshot.params.id,this.book)
      .subscribe(
        (data:any) => {
          this.router.navigateByUrl('/home');
        }
      )
    });
    
  }
  login() {
    this.router.navigateByUrl('/login');
  }
  
  register() {
    this.router.navigateByUrl('/register');
  }

  logout() {
    this.logged = false;
    localStorage.setItem("logged","false");
    localStorage.setItem("user","null");
    // window.location.reload();
    this.router.navigateByUrl('/home');
  }

  getRole(user){
    if(user.role == "admin"){
      this.admin = true;
    }
    else{
      this.admin = false;
    }
  }

  home() {
    this.router.navigateByUrl('/home');
  }
}
