import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public title = "Bienvenido otra vez";
  public user:User;
  hide = true;
  model: any = {};
  logged: boolean;
  localUrl = "http://localhost:8081/login"
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  constructor(private http: HttpClient,private authService: AuthService,private router: Router) {
    this.user = new User("","","","");
   }

  ngOnInit() {
  }

  login() {
    // let user;
    // let url = 'http://localhost:8080/login';
    // console.log(this.user);

    this.http.post(this.localUrl,this.user)
    .subscribe(
      (response:any) => {
        if(response === false){
          alert("Error al registrar el usuario");
        }
        else{
          this.router.navigateByUrl('/home');
          localStorage.setItem("user",JSON.stringify(response));
          localStorage.setItem("logged","true");
        }
     }
    //  ,error  => { alert("Error al registrar el usuario");}
    )
    
  }


onSubmit(){
  this.http.post(this.localUrl,this.user)
    .subscribe(
      (response:any) => {
        if(response === false){
          alert("Error al registrar el usuario");
        }
        else{
          this.router.navigateByUrl('/home');
          localStorage.setItem("user",JSON.stringify(response));
          localStorage.setItem("logged","true");
        }
     }
    //  ,error  => { alert("Error al registrar el usuario");}
    )
}

ingresar(proveedor){
  let socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;

 this.authService.signIn(socialPlatformProvider)
 .then((userData) => {
    this.sendToRestApiMethod(userData);
 });
}

sendToRestApiMethod(userData) : void {
  let token = userData.idToken;
  let user = new User(userData.name,userData.id,userData.email,userData.role);
  

  this.http.post(this.localUrl,user)
    .subscribe(
      (response:any) => {
        if(response == false){
          alert("Error en el usuario");
        }
        else{
          this.router.navigateByUrl('/home');
          localStorage.setItem("user",JSON.stringify(response));
          localStorage.setItem("logged","true");
        }
     }
    //  ,error  => { alert("Error al registrar el usuario");}
    )
}

  register() {
    this.router.navigateByUrl('/register');
  }

  home() {
    this.router.navigateByUrl('/');
  }
}
