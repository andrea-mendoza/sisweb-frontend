import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  public title = "Registrar Usuario";
  public user:User;
  hide = true; 
  localUrl = "http://localhost:8081/newUser";

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  
  constructor(private http: HttpClient,private authService: AuthService,private router: Router) {
    this.user = new User("","","","user");
   }

  ngOnInit() {
  }

      

  onSubmit(){
    this.http.post(this.localUrl,this.user)
    .subscribe(
      (response:any) => {
        // console.log(response);
        this.router.navigateByUrl('/home');
        localStorage.setItem("user",JSON.stringify(response));
        localStorage.setItem("logged","true");
     },error  => { alert("Error al registrar el usuario");}
    )
  }

  ingresar(proveedor){
    let socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;

   this.authService.signIn(socialPlatformProvider)
   .then((userData) => {
      this.sendToRestApiMethod(userData);
   });
  }

  sendToRestApiMethod(userData) : void {
    let token = userData.idToken;
    let user = new User(userData.name,userData.id,userData.email,"user");
    

    this.http.post(this.localUrl,user)
    .subscribe(
      (response:any) => {
        this.router.navigateByUrl('/home');
        // console.log(response);
        localStorage.setItem("user",JSON.stringify(response));
        localStorage.setItem("logged","true");
     },error  => { alert("Error al registrar el usuario");}
    )
 }
 login() {
  this.router.navigateByUrl('/login');
  }

  home() {
    this.router.navigateByUrl('/');
  }
}
