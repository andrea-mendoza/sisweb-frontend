// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDTKs44UIT61Ge5PDOh2gnslur7fqwzUko",
    authDomain: "sisweb-proyecto.firebaseapp.com",
    databaseURL: "https://sisweb-proyecto.firebaseio.com",
    projectId: "sisweb-proyecto",
    storageBucket: "sisweb-proyecto.appspot.com",
    messagingSenderId: "62354747681",
    appId: "1:62354747681:web:c10c93f55cb772fe"
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
